<?php


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates');

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$app->get('/', function (Request $request, Response $response) {
    return $this->view->render($response, 'index.html', [
        'name' => '<script type="text/javascript">console.log("I am l88t af");</script>'
    ]);

})->setName('index');

$app->get('/phpinfo', function (Request $request, Response $response) {
    phpinfo();

})->setName('phpinfo');

$app->get('/testmail', function (Request $request , Response $response) {


    $mail = new PHPMailer;

    $mail->SMTPDebug = 0;                               // Enable verbose debug output
    //$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'localhost';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = null;                 // SMTP username
    $mail->Password = null;                           // SMTP password
//     $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 1025;                                    // TCP port to connect to

    $mail->setFrom('from@example.com', 'Mailer');
    $mail->addAddress('katie@thetomorrowlab.com', 'Joe User');

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Here is a subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }

})->setName('testmail');


$app->run();
