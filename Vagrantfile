# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 1.9.1"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|


  unless Vagrant.has_plugin?('vagrant-auto_network')
    raise 'You need to install vagrant-auto_network, or this will not work'
  end

  unless Vagrant.has_plugin?('vagrant-hostmanager')
    raise 'vagrant-hostmanager is not installed!'
  end


  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"

#  config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
#     if hostname = (vm.ssh_info && vm.ssh_info[:host])
#        `vagrant ssh -c "/sbin/ifconfig eth1" | grep "inet addr" | tail -n 1 | egrep -o "[0-9\.]+" | head -n 1 2>&1`.split("\n").first[/(\d+\.\d+\.\d+\.\d+)/, 1]
#      end
#    end
  config.vm.provision :hostmanager
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false


server_cpus           = "1"   # Cores
server_memory         = "1024" # MB
hostname              = "single-page-site"

   config.vm.provider "virtualbox" do |vb|

    vb.name = hostname
    config.vm.hostname = hostname

     # Customize the amount of memory on the VM:
    vb.customize ["modifyvm", :id, "--cpus", server_cpus]

    # Set server memory
    vb.customize ["modifyvm", :id, "--memory", server_memory]

   end

    # Create a static IP
  config.vm.network :private_network, :ip => "0.0.0.0", :auto_network => true


  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.

  config.vm.synced_folder ".", "/vagrant",
    id: "core",
    :nfs => true,
    :mount_options => ['nolock,vers=3,udp,noatime,actimeo=2,fsc']

  config.vm.synced_folder "../dev-certs", "/dev-certs",
    id: "dev-certs",
    :nfs => true,
    :mount_options => ['nolock,vers=3,udp,noatime,actimeo=2,fsc']

    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  #Making an assumption that we'll always want php7 & nginx
    config.vm.provision "shell", inline: "sudo apt-get update;"
    config.vm.provision "shell", path: "helper-scripts/fix-locale.sh"
    config.vm.provision "shell", path: "helper-scripts/make-ssl.sh", args: "single-page-site"
    config.vm.provision "shell", path: "helper-scripts/update-packages.sh"
    config.vm.provision "shell", path: "helper-scripts/php7-installer.sh"
    config.vm.provision "shell", path: "helper-scripts/maria-db-installer.sh"
    config.vm.provision "shell", path: "helper-scripts/nginx-installer.sh"
    config.vm.provision "shell", path: "helper-scripts/add-gitignore.sh"
config.vm.provision "shell", path: "helper-scripts/install-git.sh"
config.vm.provision "shell", path: "helper-scripts/install-composer.sh"
config.vm.provision "shell", path: "helper-scripts/nginx-add-host.php", args: "single-page-site /vagrant/site/public"
config.vm.provision "shell", path: "helper-scripts/xdebug/install-xdebug.sh"
    config.vm.provision "shell", inline: "sudo systemctl start nginx", run: "always"
    config.vm.provision "shell", path: "helper-scripts/install-mailcatcher.sh"
    config.vm.provision "shell", inline: "/usr/bin/env $(which mailcatcher) --ip=0.0.0.0", run: "always"
end